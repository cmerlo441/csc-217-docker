FROM ubuntu:latest
LABEL maintainer="Christopher R. Merlo <christopher.merlo@ncc.edu>"
LABEL version="2023.07"

RUN apt-get update && \
    apt-get install -y \
    build-essential \
    clang-format \
    curl \
    fonts-powerline \
    gdb \
    git-core \
    gnupg \
    locales \
    nano \
    manpages-dev \
    sudo \
    wget \
    && locale-gen en_US.UTF-8
RUN useradd -m csc217 && echo "csc217:csc217" | chpasswd && adduser csc217 sudo

RUN sh -c "$(wget -O- https://github.com/deluan/zsh-in-docker/releases/download/v1.1.5/zsh-in-docker.sh)" \
    -t agnoster \
    -p git \
    -p https://github.com/zsh-users/zsh-syntax-highlighting \
    -p https://github.com/zsh-users/zsh-autosuggestions \
    -p https://github.com/zsh-users/zsh-completions

RUN mv /root/.oh-my-zsh /home/csc217/

COPY .zshrc /home/csc217/.zshrc

USER csc217
WORKDIR /home/csc217
ENV TERM xterm
ENV ZSH_THEME agnoster
CMD ["zsh"]
